import requests
import argparse
import datetime
import logging
import sqlalchemy

# XXX:設定ファイルに切り出す
conf_apikey = {
    'VT_APIKEY_01': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_02': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_03': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_04': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_05': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_06': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_07': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_08': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_09': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_10': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_11': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_12': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_13': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_14': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    'VT_APIKEY_00': "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
}

# XXX:DB連携させる


def connect_database():
    pass


def regist_malicius_info():
    pass


def vt_file_result(apikey: str, resource: str):
    url = 'https://www.virustotal.com/vtapi/v2/file/report'
    params = {'apikey': apikey, 'resource': resource}
    response = requests.get(url, params=params)
    return response.json()


def vt_url_result(apikey: str, resource: str):
    url = 'https://www.virustotal.com/vtapi/v2/url/report'
    params = {'apikey': 'apikey', 'resource': resource}
    response = requests.get(url, params=params)
    return response.json()


def read_apikey():
    """
     実行時間の秒数から計算し、APIKEYを取得する
    """
    datetime_now = datetime.datetime.now()
    api_num = '{0:02d}'.format(datetime_now.second % 15)
    apikey = conf_apikey["VT_APIKEY_%s" % api_num]
    return apikey


def args_check():
    usage = 'python {} [-u  <URL> ] [-f <filehash>] [--help]'.format(__file__)
    description = "discription: A script that circumvents the virus total's pubric API's 15 seconds / times limit with a trick."
    parser = argparse.ArgumentParser(
        prog='__file__', usage=usage, description=description)
    parser.add_argument('-u', '--url-result',
                        type=str,
                        help='Get VirusTotal URL Scan Results',
                        action='append')
    parser.add_argument('-f', '--filehash-result',
                        type=str,
                        help='Get VirusTotal File Scan Results',
                        action='append')
    parser.add_argument('-uL', '--url-list',
                        type=str,
                        help="（Unimplemented）Collecting VirusScan's URL Scan Results Collectively",
                        action='append')
    parser.add_argument('-fL', '--filehash-list',
                        type=str,
                        help="（Unimplemented）Collecting VirusScan's URL Scan Results Collectively",
                        action='append')
    args = parser.parse_args()
    return args, usage


def main():
    option, usage = args_check()
    apikey = read_apikey()
    if option.url_result:
        response = vt_url_result(apikey, option.url_result[0])
        print(response)
    elif option.filehash_result:
        response = vt_file_result(apikey, option.filehash_result[0])
        print(response)
    else:
        print("usage:" + usage)
    # XXX:ローカルDB登録
    # regist_malicius_info(response)
    pass


if __name__ == "__main__":
    main()
